package io.overcoded.vaadin.wizard;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.textfield.IntegerField;
import lombok.Getter;

import java.util.Objects;

public class TestStep extends WizardStep<TestContext> {
    @Getter
    private final Div layout;
    private final IntegerField field;

    public TestStep(TestContext context, String name, int order) {
        super(context, name, order);
        layout = new Div();
        field = new IntegerField("Number");
        configureLayout();
        configureField();
    }

    private void configureLayout() {
        layout.add(new H3("View for step: " + getName()));
    }

    private void configureField() {
        field.setMin(0);
        field.setMax(10);
        field.setStep(1);
        field.setValue(0);
        field.setErrorMessage("Please set a number between 0 and 10.");
        layout.add(field);
    }

    @Override
    public boolean isValid() {
        // Validate the necessary fields
        boolean result = Objects.nonNull(field.getValue());
        if (!result) {
            field.setInvalid(true);
        }
        return result;
    }

    @Override
    public boolean commit() {
        // Do something with the context
        context.getNumbers().add(field.getValue());
        layout.setEnabled(false);
        return true;
    }
}
