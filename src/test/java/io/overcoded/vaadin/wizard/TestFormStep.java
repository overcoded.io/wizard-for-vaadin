package io.overcoded.vaadin.wizard;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.validator.EmailValidator;

public class TestFormStep extends AbstractFormWizardStep<TestContext> {
    protected TestFormStep(TestContext context, String name, int order) {
        super(context, name, order);
    }

    @Override
    protected String getDisplayEntity() {
        return context.getEmail();
    }

    @Override
    protected boolean commitAfterContextUpdated() {
        return true;
    }

    @Override
    protected Html getInformationMessage() {
        return new Html("<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent gravida, tellus at <strong>condimentum porttitor</strong>,leo eros vulputate lorem, at vestibulum mi nulla vel ligula. Maecenas placerat <u>orci nec velit</u> aliquet facilisis. Praesent pretium quis orci id consectetur. <strong>Sed sodales volutpat leo</strong>, in dapibus felis faucibus non. Fusce eget lorem sit amet erat volutpat rutrum ut id ex. Donec nec eleifend arcu, at congue libero. Mauris non enim faucibus, sodales enim sed, fringilla lectus. Nam dapibus nulla in sodales mollis. </p>");
    }

    @Override
    protected FormLayout createForm() {
        TextField firstName = new TextField("First name");
        firstName.setRequiredIndicatorVisible(true);
        TextField lastName = new TextField("Last name");
        lastName.setRequiredIndicatorVisible(true);
        EmailField email = new EmailField("Email");
        email.setRequiredIndicatorVisible(true);

        FormLayout formLayout = new FormLayout();

        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("500px", 2)
        );
        formLayout.add(firstName, lastName, email);
        formLayout.setColspan(email, 2);

        binder.forField(firstName)
                .asRequired("Please set the first name")
                .bind("firstName");
        binder.forField(lastName)
                .asRequired("Please set the last name")
                .bind("lastName");
        binder.forField(email)
                .asRequired("Please set the email address")
                .withValidator(new EmailValidator("Please set a valid email address", true))
                .bind("email");

        binder.readBean(context);

        return formLayout;
    }
}
