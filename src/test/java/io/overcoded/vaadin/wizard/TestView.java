package io.overcoded.vaadin.wizard;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.Route;
import io.overcoded.vaadin.wizard.config.WizardConfigurationProperties;
import io.overcoded.vaadin.wizard.config.WizardFooterConfigurationProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Route("")
public class TestView extends Div {
    public TestView() {
        setSizeFull();
        TestContext context = new TestContext();
        // lets override a default config and hide the back button until we don't have completed step
        WizardConfigurationProperties properties = new WizardConfigurationProperties();
        properties.getFooter().setBackButtonOnFirstStep(WizardFooterConfigurationProperties.ButtonStatus.HIDDEN);

        // define steps, you need to implement each of your steps (extend WizardStep)
        List<WizardStep<TestContext>> generatedSteps = IntStream
                .range(2, 7)
                .mapToObj(number -> new TestStep(context, "Step #" + number, number))
                .map(testStep -> (WizardStep<TestContext>) testStep)
                .toList();

        List<WizardStep<TestContext>> steps = new ArrayList<>();
        steps.add(new TestFormStep(context, "Step #1", 1));
        steps.addAll(generatedSteps);
        Wizard<TestContext> wizard = new Wizard<>(properties, context, steps);
        // we want to inherit the configuration from the wizard
        wizard.createFinishLayout(TestFinishView::new);
        add(wizard);
    }
}
