package io.overcoded.vaadin.wizard;

interface WizardPager {
    /**
     * Starting the wizard (initializing the pager and jumping to the first step)
     */
    void start();

    /**
     * Going back to the previous step
     */
    void previous();

    /**
     * Validating, committing the current step and everything fine, going to the next step
     */
    void next();

    /**
     * Finalizing the wizard
     */
    void finish();
}
