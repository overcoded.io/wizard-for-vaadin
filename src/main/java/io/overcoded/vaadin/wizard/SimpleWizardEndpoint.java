package io.overcoded.vaadin.wizard;

import com.vaadin.flow.component.orderedlayout.Scroller;
import io.overcoded.vaadin.wizard.config.WizardSimpleContentConfigurationProperties;

public class SimpleWizardEndpoint<T> extends AbstractWizardEndpoint<T> {
    public SimpleWizardEndpoint(T context) {
        this(new WizardSimpleContentConfigurationProperties(), context);
    }

    public SimpleWizardEndpoint(WizardSimpleContentConfigurationProperties properties, T context) {
        super(properties, context);
        setSizeFull();
        Scroller scroller = new Scroller(getLayout());
        scroller.setWidthFull();
        scroller.setScrollDirection(Scroller.ScrollDirection.VERTICAL);
        add(scroller);
    }
}
