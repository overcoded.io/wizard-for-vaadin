package io.overcoded.vaadin.wizard.config;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.IconFactory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WizardButtonConfigurationProperties {
    /**
     * Text of the button. Null or empty text means it's an icon only button.
     * If you want to hide text, just set this to null. You may need to add LUMO_ICON theme variant.
     */
    private String text;
    /**
     * Icon of the button. Null means the button doesn't have icon.
     */
    private IconFactory icon;
    /**
     * Configuration of the button.
     */
    private List<ButtonVariant> variants;
    /**
     * Icon should be displayed after the text (or before the text)
     */
    private boolean iconAfterText;
}
