package io.overcoded.vaadin.wizard.config;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import lombok.Data;

import java.util.List;

@Data
public class WizardFooterConfigurationProperties {
    /**
     * Override background color of footer
     */
    private String backgroundColor = "var(--lumo-contrast-5pct)";
    /**
     * Turn padding on or off
     */
    private boolean paddingEnabled = true;
    /**
     * If true, back button will be visible to allow users to step back to the previous step
     */
    private boolean backwardSteppingEnabled = true;
    /**
     * In case of first step, there is no valid "back" action.
     * With this configuration you can hide the back button, or simply deactivate it.
     */
    private ButtonStatus backButtonOnFirstStep = ButtonStatus.DISABLED;
    /**
     * In case of last step, there is no valid "next" action.
     * With this configuration you can hide the next button, or simple deactivate it.
     */
    private ButtonStatus nextButtonBeforeFirstStep = ButtonStatus.HIDDEN;
    /**
     * In case of last step, there is no valid "next" action.
     * With this configuration you can hide the next button, or simple deactivate it.
     */
    private ButtonStatus nextButtonOnLastStep = ButtonStatus.HIDDEN;
    /**
     * In case of any step, the is no valid "start" action.
     * With this configuration you can hide the start button, or simple deactivate it.
     */
    private ButtonStatus startButtonOnSteps = ButtonStatus.HIDDEN;
    /**
     * In case of any step, except the latest one, the is no valid "finish" action.
     * With this configuration you can hide the finish button, or simple deactivate it.
     */
    private ButtonStatus finishButtonOnRegularSteps = ButtonStatus.HIDDEN;
    /**
     * Enable icon support for buttons
     */
    private boolean iconEnabled = true;
    /**
     * Configuration of back button
     */
    private WizardButtonConfigurationProperties backButton = WizardButtonConfigurationProperties.builder()
            .text("Back")
            .icon(VaadinIcon.ARROW_BACKWARD)
            .build();
    /**
     * Configuration of start button
     */
    private WizardButtonConfigurationProperties startButton = WizardButtonConfigurationProperties.builder()
            .text("Start")
            .icon(VaadinIcon.MAGIC)
            .variants(List.of(ButtonVariant.LUMO_PRIMARY))
            .build();
    /**
     * Configuration of next button
     */
    private WizardButtonConfigurationProperties nextButton = WizardButtonConfigurationProperties.builder()
            .text("Next")
            .iconAfterText(true)
            .icon(VaadinIcon.ARROW_FORWARD)
            .variants(List.of(ButtonVariant.LUMO_PRIMARY))
            .build();
    /**
     * Configuration of finish button
     */
    private WizardButtonConfigurationProperties finishButton = WizardButtonConfigurationProperties.builder()
            .text("Finish")
            .icon(VaadinIcon.CHECK)
            .variants(List.of(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SUCCESS))
            .build();


    public enum ButtonStatus {
        DISABLED, HIDDEN
    }
}
