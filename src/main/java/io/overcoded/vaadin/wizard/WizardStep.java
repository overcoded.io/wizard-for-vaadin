package io.overcoded.vaadin.wizard;

import com.vaadin.flow.component.avatar.AvatarGroup;
import com.vaadin.flow.component.html.Div;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(of = "name")
@EqualsAndHashCode(of = "name")
public abstract class WizardStep<T> {
    private final String name;
    private final int order;
    private StepStatus status;
    private boolean completed;
    private boolean committed;
    protected final T context;

    protected WizardStep(T context, String name, int order) {
        this.name = name;
        this.order = order;
        this.context = context;
        this.status = StepStatus.INACTIVE;
        this.completed = false;
        this.committed = false;
    }

    public abstract Div getLayout();

    public abstract boolean isValid();

    public abstract boolean commit();

    public AvatarGroup.AvatarGroupItem createAvatarGroupItem() {
        AvatarGroup.AvatarGroupItem item = new AvatarGroup.AvatarGroupItem();
        item.setName(name);
        item.setAbbreviation(String.valueOf(order));
        item.setColorIndex(calculateColorIndex());
        return item;
    }

    private int calculateColorIndex() {
        return status.getColorIndex() + shiftWhenCompletedAndActive();
    }

    private int shiftWhenCompletedAndActive() {
        return completed && status == StepStatus.ACTIVE
                ? 2
                : 0;
    }
}
