package io.overcoded.vaadin.wizard;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.binder.ValidationResult;
import io.overcoded.vaadin.wizard.config.WizardContentConfigurationProperties;
import io.overcoded.vaadin.wizard.config.WizardInformationConfigurationProperties;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
public abstract class AbstractFormWizardStep<T> extends WizardStep<T> {
    protected final WizardContentConfigurationProperties properties;
    protected final Binder<T> binder;

    protected AbstractFormWizardStep(T context, String name, int order) {
        this(new WizardContentConfigurationProperties(), context, name, order);
    }

    protected AbstractFormWizardStep(WizardContentConfigurationProperties properties, T context, String name, int order) {
        super(context, name, order);
        this.properties = properties;
        this.binder = new Binder<>((Class<T>) context.getClass());
    }

    @Override
    public Div getLayout() {
        Div layout = new Div();
        configureLayout(layout);
        return layout;
    }

    protected void configureLayout(Div layout) {
        FormLayout formLayout = createForm();
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        if (properties.getInformation().isEnabled()) {
            Details details = createDetails();
            verticalLayout.add(wrappedDetails(details));
        }
        verticalLayout.add(wrappedFormLayout(formLayout));
        verticalLayout.setAlignItems(properties.getAlignItems());
        layout.add(verticalLayout);
        layout.setSizeFull();
    }

    @Override
    public boolean isValid() {
        BinderValidationStatus<T> validate = binder.validate();
        if (log.isDebugEnabled()) {
            String validationErrors = validate.getValidationErrors().stream().map(ValidationResult::getErrorMessage).collect(Collectors.joining("; "));
            log.info("{} step (#{}) validation result is {}. Validation error messages: {}", getName(), getOrder(), validate.isOk(), validationErrors);
        } else {
            log.info("{} step (#{}) validation result is {}.", getName(), getOrder(), validate.isOk());
        }
        return validate.isOk();
    }

    @Override
    public boolean commit() {
        boolean result = false;
        try {
            binder.writeBean(context);
            result = commitAfterContextUpdated();
            log.info("{} step (#{}) has been completed{}. Selected entity: {}", getName(), getOrder(), getCompletedByMessage(), getDisplayEntity());
        } catch (ValidationException ex) {
            log.debug("Failed to validate website selection: {}", ex.getMessage());
        }
        return result;
    }

    protected String getCompletedByMessage() {
        String username = getUsername();
        return Objects.nonNull(username) && !username.isBlank() ? " by " + username : "";
    }

    /**
     * If username is not null and not black, the log can display this user. So you will know how is working on a step.
     */
    protected String getUsername() {
        return null;
    }

    /**
     * Each step represents a unit. This is the string representation of this unit.
     * Will be used for logging purpose.
     */
    protected abstract String getDisplayEntity();

    /**
     * The inherited `commit` method only updates the necessary fields in the context, if the form is valid.
     * After that it's calling this method, so you can implement your custom commit logic here.
     * If this method return false, means the commit failed, so the user can't proceed to the next step.
     */
    protected abstract boolean commitAfterContextUpdated();

    /**
     * Html content of your information message. If you disable the information message, you can return
     * null or empty html tag.
     */
    protected abstract Html getInformationMessage();

    /**
     * The FormLayout which represents the form. This method should define the fields on the form, and also
     * configure the binder for these fields.
     */
    protected abstract FormLayout createForm();

    /**
     * If you want a small form to be aligned to the center, you need to wrap the
     * form layout into a HorizontalLayout. You also can implement any custom logic around your form layout.
     */
    protected Component wrappedFormLayout(FormLayout formLayout) {
        return formLayout;
    }

    /**
     * This method is wrapping the information details into a layout and applying the
     * configured style on it (background, padding, border, etc.). You can override it
     * to display details as you want.
     */
    protected Component wrappedDetails(Details details) {
        WizardInformationConfigurationProperties informationProperties = properties.getInformation();
        HorizontalLayout wrapper = new HorizontalLayout(details);
        wrapper.setWidthFull();
        if (Objects.nonNull(informationProperties.getBackgroundColor()) && !informationProperties.getBackgroundColor().isBlank()) {
            wrapper.setPadding(true);
            wrapper.getStyle().set("background-color", informationProperties.getBackgroundColor());
        }
        if (Objects.nonNull(informationProperties.getBorderColor()) && !informationProperties.getBorderColor().isBlank()) {
            wrapper.setPadding(true);
            wrapper.getStyle().set("border", "1px solid " + informationProperties.getBorderColor());
            wrapper.getStyle().set("border-radius", informationProperties.getBorderRadius());
        }
        return wrapper;
    }

    protected Details createDetails() {
        WizardInformationConfigurationProperties informationProperties = properties.getInformation();
        HorizontalLayout summary = new HorizontalLayout();
        summary.setSpacing(false);

        Icon icon = informationProperties.getIcon().create();
        icon.getStyle().set("width", "var(--lumo-icon-size-s)");
        icon.getStyle().set("height", "var(--lumo-icon-size-s)");

        HorizontalLayout infoBadge = new HorizontalLayout(icon);
        infoBadge.setSpacing(false);
        infoBadge.getStyle().set("color", informationProperties.getIconColor());
        infoBadge.getStyle().set("margin-right", "var(--lumo-space-s)");

        summary.add(infoBadge, new Text(informationProperties.getText()));

        HorizontalLayout content = new HorizontalLayout();
        content.add(getInformationMessage());

        Details details = new Details(summary, content);
        details.setWidthFull();
        details.setOpened(informationProperties.isOpened());

        return details;
    }
}
