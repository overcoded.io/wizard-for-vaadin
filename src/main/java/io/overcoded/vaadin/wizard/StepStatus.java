package io.overcoded.vaadin.wizard;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum StepStatus {
    ACTIVE(2),
    INACTIVE(3),
    COMPLETED(1);
    private final int colorIndex;
}
