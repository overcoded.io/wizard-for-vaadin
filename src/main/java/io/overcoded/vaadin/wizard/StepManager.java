package io.overcoded.vaadin.wizard;

import java.util.List;

class StepManager<T> {
    private final List<WizardStep<T>> steps;
    private WizardStep<T> activeStep;
    private int activeIndex;

    StepManager(List<WizardStep<T>> steps) {
        this.steps = steps;
    }

    public List<WizardStep<T>> getSteps() {
        return steps;
    }

    public WizardStep<T> getActiveStep() {
        return activeStep;
    }

    void start() {
        initSteps();
    }

    void previous() {
        switchTo(activeIndex - 1);
        setActive();
    }

    boolean next() {
        boolean valid = false;
        if (activeStep.isValid() && activeStep.commit()) {
            setCompleted();
            switchTo(activeIndex + 1);
            setActive();
            valid = true;
        }
        return valid;
    }

    boolean finish() {
        boolean valid = false;
        if (!activeStep.isCommitted()) {
            if (activeStep.isValid() && activeStep.commit()) {
                setCompleted();
                activeIndex = steps.size();
                valid = true;
            }
        } else {
            setCompleted();
            activeIndex = steps.size();
            valid = true;
        }
        return valid;
    }

    private void initSteps() {
        activeIndex = 0;
        if (!steps.isEmpty()) {
            activeStep = steps.get(activeIndex);
            setActive();
        }
    }

    private void switchTo(int newIndex) {
        if (newIndex < steps.size() && newIndex >= 0) {
            activeStep = steps.get(newIndex);
            activeIndex = newIndex;
        }
    }

    private void setCompleted() {
        activeStep.setStatus(StepStatus.COMPLETED);
        activeStep.setCompleted(true);
    }

    private void setActive() {
        activeStep.setStatus(StepStatus.ACTIVE);
    }
}
