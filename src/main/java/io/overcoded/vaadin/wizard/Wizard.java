package io.overcoded.vaadin.wizard;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import io.overcoded.vaadin.wizard.config.WizardConfigurationProperties;
import io.overcoded.vaadin.wizard.config.WizardSimpleContentConfigurationProperties;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.function.BiFunction;

@Tag("overcoded-wizard")
public class Wizard<T> extends Div {
    @Getter
    private final transient WizardConfigurationProperties properties;
    private final transient StepManager<T> stepManager;
    private final transient WizardPager pager;
    protected final transient T context;
    private final VerticalLayout layout;
    @Setter
    private AbstractWizardEndpoint<T> finishLayout;
    @Setter
    private AbstractWizardEndpoint<T> startLayout;

    public Wizard(T context, List<WizardStep<T>> steps) {
        this(new WizardConfigurationProperties(), context, steps);
    }

    public Wizard(WizardConfigurationProperties properties, T context, List<WizardStep<T>> steps) {
        this.properties = properties;
        this.stepManager = new StepManager<>(steps);
        this.pager = new InternalWizardPager<>(this);
        this.layout = new VerticalLayout();
        this.context = context;
        this.startLayout = new SimpleWizardEndpoint<>(properties.getContent().getStartView(), context);
        this.finishLayout = new SimpleWizardEndpoint<>(properties.getContent().getFinishView(), context);
        layout.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        layout.setPadding(false);
        layout.setSpacing(false);
        layout.setSizeFull();
        setSizeFull();
        add(layout);
        if (properties.isStartViewEnabled()) {
            setStartLayout();
        } else {
            start();
        }
    }

    public void createFinishLayout(BiFunction<WizardSimpleContentConfigurationProperties, T, AbstractWizardEndpoint<T>> supplier) {
        finishLayout = supplier.apply(properties.getContent().getFinishView(), context);
    }

    public void createStartLayout(BiFunction<WizardSimpleContentConfigurationProperties, T, AbstractWizardEndpoint<T>> supplier) {
        startLayout = supplier.apply(properties.getContent().getStartView(), context);
    }

    private void start() {
        stepManager.start();
        setLayout();
    }

    private void previous() {
        stepManager.previous();
        setLayout();
    }

    private void next() {
        if (stepManager.next()) {
            setLayout();
        }
    }

    private void finish() {
        if (stepManager.finish()) {
            layout.removeAll();
            layout.add(getHeader());
            layout.add(finishLayout);
        }
    }

    private void setLayout() {
        layout.removeAll();
        layout.add(getHeader());
        layout.add(getContent());
        layout.add(getFooter());
    }

    private void setStartLayout() {
        layout.add(getHeader());
        layout.add(startLayout);
        layout.add(getFooter());
    }

    private WizardHeader<T> getHeader() {
        return new WizardHeader<>(properties.getHeader(), stepManager.getSteps());
    }

    private WizardContent<T> getContent() {
        return new WizardContent<>(stepManager.getActiveStep());
    }

    private WizardFooter<T> getFooter() {
        return new WizardFooter<>(properties.getFooter(), stepManager.getSteps(), pager);
    }

    @RequiredArgsConstructor
    private static final class InternalWizardPager<U> implements WizardPager {
        private final Wizard<U> wizard;

        @Override
        public void start() {
            wizard.start();
        }

        @Override
        public void previous() {
            wizard.previous();
        }

        @Override
        public void next() {
            wizard.next();
        }

        @Override
        public void finish() {
            wizard.finish();
        }
    }
}
