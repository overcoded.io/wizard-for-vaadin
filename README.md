# Wizard for Vaadin
Simple way to define multistep wizards with Vaadin

![Wizard preview](preview.png)

## Usage

### 1. Define your context

Create a class, which will hold all the necessary entities for you through your steps.
If you create multiple wizards, this class may be wizard specific.
It can contain your most important entities or data which should be available
on different steps or on the final screen.

### 2. Define your steps

You need to implement `WizardStep`. Normally each step in your wizard should be
a separate class. Each step should get the same context instance in the
constructor.

#### The "hard" way

In `WizardStep` there is a `Div`, named `layout`. You can use this `layout` as a holder of the UI
of your step. As the status of the content depends on how users reaches the step (what the user)
selected in the previous step, or maybe the user is going backward, it's preferred to  
`protected void onAttach(AttachEvent attachEvent)`

To make your wizard work, you need to implement the following method in your steps:

- `Div getLayout()`: to return the layout which should be displayed
- `boolean isValid()`: if the user clicks on the next button, this method will validate your step
- `boolean commit()`: if the validation passed, you can commit your change: save to database, add to the context, etc.

If any of `isValid()` or `commit()` methods are failing, the wizard won't go to the next step.

#### The simple way

If you want to create a simple form based step, with a details message (like on the preview image),
you only need to extend `AbstractFormWizardStep` (just like in `TestFormStep`).
You only need to define your information message, your form layout and what to do after validation successes
and the context has been updated with the fields. The validation uses Vaadin's Binder, so you also need to bind your
fields to your context.

Here is an example custom step:
![Default start view](images/form-step-custom.png)

To make your form based wizard work, you need to implement the following method in your step:

- `boolean commitAfterContextUpdate()`: the method is part is called by the `commit()` method in `WizardStep` after the
  validation happened. You can save your entity to the database here, or do anything with the context after the binder
  updated the fields in the context.
- `Html getInformationMessage()`: the content of the information details. If you don't need information message, it can
  be disabled from configuration.
- `FormLayout createForm()`: the place where you need to implement your form (and bind your fields with the binder for
  validation)

### 3. Instantiate your steps

After you've created all your step implementation, create a new instance from them and collect them
into a list.

### 4. Define your wizard

If the default configurations are fine for you, you simply can create a new `Wizard` instance. You
only need to define the `context` (same you set for your steps) and your list of steps.

Add your `Wizard` instance to your layout.

### 5. Set your custom start layout (optional)

There is a configurable built-in default start layout. But you also can define your own start view. The wizard will
only start if the user clicks to the start button. This view is useful is you want to display some extra
information before start.

The default start view looks like this:
![Default start view](images/start-view-default.png)

After you designed your start view, you only need to set it for your `Wizard` by calling `setStartLayout()` on it
or if you base your view on `AbstractWizardEndpoint` you can inherit configuration from the `Wizard` by calling
`createStartLayout` method. If you choose this option, you also can
implement `Optional<Component> getExtraBottomContent()`
method which makes it possible to add extra content to the bottom part, after the message.

If you simply want to configure the built-in start view, you can configure the following:

- icon and icon size, if you want to display an icon
- picture (name, path, width, height, padding) if you want to display a picture instead of the icon
- title (and title padding if necessary)
- color, which will be the background color of the top part and the border color of the bottom part
- contrast color, which will be used as text color where we applied background color
- border radius
- the width of the message box
- message, an HTML text which should be displayed at the bottom part

### 6. Set your custom finish layout (optional)

There is a configurable built-in default finish layout. But you also can define your own finish view.
You only need to extend the `AbstractWizardEndpoint` class with the same context you used for your steps and wizard.

After you designed your finish view, you only need to set it for your `Wizard` by calling `setFinishLayout()` on it
or if you base your view on `AbstractWizardEndpoint` you can inherit configuration from the `Wizard` by calling
`createFinishLayout` method. If you choose this option, you also can
implement `Optional<Component> getExtraBottomContent()`
method which makes it possible to add extra content to the bottom part, after the message.

A custom end view with the implemented `getExtraBottomContent()` method:
![Default start view](images/end-view-custom.png)

If you simply want to configure the built-in start view, you can configure the following:

- icon and icon size, if you want to display an icon
- picture (name, path, width, height, padding) if you want to display a picture instead of the icon
- title (and title padding if necessary)
- color, which will be the background color of the top part and the border color of the bottom part
- contrast color, which will be used as text color where we applied background color
- border radius
- the width of the message box
- message, an HTML text which should be displayed at the bottom part

## Configuration

Wizard for Vaadin contains some configuration, so you can change the look of your wizard, mostly the header and the
footer.

### Wizard configuration

Under `WizardConfigurationProperties` you can configure the following:

- the header & footer configurations
- start view should be displayed or not

### Header configuration
Under `WizardHeaderConfigurationProperties` you can configure the following:

- background color of header
- order of columns (completed, active and inactive)
- the title of columns
- display titles or not
- display title when there is no related task
- color of task avatars
- too many tasks in a column should be grouped or not
- if yes, after how many items should we group them

For more details, please check the `WizardHeaderConfigurationProperties` class.

### Content configuration
Under `WizardContentConfigurationProperties` you can configure the following:

- is the information details enabled or not
- the icon for the summary
- the color of the icon in the summary
- the text of the summary
- should the details be opened automatically or not
- the background, border color and border radius of the details

Besides the start and end views, content can be configured as described above. 

### Footer configuration

Under `WizardFooterConfigurationProperties` you can configure the following:

- background color of footer
- is padding enabled or not
- backward step is enabled or not
- icon should be displayed on buttons or not
- how to handle unavailable button: disable them or hide them (e.g.: finish button when there is multiple incomplete
  steps)
- how should different buttons look (text, icon, vaadin theme variants and icon before text or after text)

For more details, please check the `WizardFooterConfigurationProperties` class.